<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'PTSneaker' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );
define( 'FS_METHOD', 'direct' );
define('FS_CHMOD_DIR', (0755 & ~ umask()));
define('FS_CHMOD_FILE', (0644 & ~ umask()));

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i/?;+E3<7W>MU2.87b.e>FYRaunv2.r$p2Q_y[,C>>n2{#*O`88m$ybSnn`%pwp<' );
define( 'SECURE_AUTH_KEY',  'O4RS#bOWom%]nu!5A-}/G2Y?sXPad_h2:>k]=36{NmwY={S]=Kd/<{dS{yDW?m,)' );
define( 'LOGGED_IN_KEY',    ':Yz<gbJ(Efd583;vH$ptZQsQaC%HX!AvNZ1=&#@OdE{=a^[ VXV`H`DQ @:Ia~.+' );
define( 'NONCE_KEY',        '}eL$JX{]Br`]UZ}JdrJ8S:/N9hSS,%oQ}3t)I0q.0A>][mvw7H8I.P-FO$XC<&EK' );
define( 'AUTH_SALT',        '*QuD@$XNAPl^?|%O.~!VQ,dd`_L.s`l(muh>wTY4[@(/gcahQ?6L}:gVr573()@{' );
define( 'SECURE_AUTH_SALT', '8.`1*CzSl:)v5-Oew:?$?t>GwW`a>(+!:+I+InBkY/ BOXQQ2siRG+5#_pyJd4.z' );
define( 'LOGGED_IN_SALT',   'v,m.%wg.aGxpK42Zli$Y-E{6FbHq9iJ%HSbSzJdzv}34L@_~7}x[N4[`c`832+ex' );
define( 'NONCE_SALT',       '+AV&+Kf&HQTozXY7FJ.bQp|m>tyW{$lj+>+)4h=[$A#F(!-D7!fGjh,,?o7~e#ZQ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
